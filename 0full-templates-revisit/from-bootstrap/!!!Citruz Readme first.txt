Made the following changes to bootstrap html pages to work inside my folder structure, in all places commented the old link with deleting for reference and added my changes below:

1.in all pages changed fav icon link 

FROM:
<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

TO:
<!-- <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png"> -->
    <link rel="shortcut icon" href="../../../ico/favicon.png">


2.in all pages changed css link 

FROM DEFAULT BOOTSTRAP CSS LINK:
	<!-- Bootstrap core CSS -->
	<link href="../../dist/css/bootstrap.css" rel="stylesheet"> -->


TO:
    <!-- Bootstrap core CSS -->
    <!-- <link href="../../dist/css/bootstrap.css" rel="stylesheet"> -->
    <!-- Citruz-Bootstrap core CSS -->
    <link href="../../../css/citruz-bootstrap.css" rel="stylesheet">

3.in all pages changed JS link 

FROM DEFAULT BOOTSTRAP.MIN JS LINK:
	<script src="../../dist/js/bootstrap.min.js"></script>


TO NEW JS FOLDER INSIDE CHITRARASU FOLDER AND NON MINIFIED FORM:
    <!-- <script src="../../dist/js/bootstrap.min.js"></script> -->
    <script src="../../../js/bootstrap.js"></script>

4.Added "docs-assets" and "ico" folder inside "chitrarasu" folder for using inside the Bootstrap Html templates.